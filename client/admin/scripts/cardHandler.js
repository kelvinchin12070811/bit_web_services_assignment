function onCardClicked(id)
{
    sessionStorage.setItem("edit-product", id);
    sessionStorage.setItem("prev-url", location.href);
    location.href = "./itemEditor.html";
}

function onAddClicked()
{
    sessionStorage.setItem("prev-url", location.href);
    location.href = "./itemEditor.html";
}