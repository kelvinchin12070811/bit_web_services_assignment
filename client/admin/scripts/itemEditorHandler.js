function onSubmit()
{
    const productID = sessionStorage.getItem("edit-product");
    const name = document.querySelector("#name").value.replace(/\s/g, "+");
    const price = document.querySelector("#price").value;
    const count = document.querySelector("#count").value;
    let category = document.querySelector("#category").value;
    let requestUrl = "";

    if (category == 1)
        category = "Game";
    else if (category == 2)
        category = "Console";
    else if (category == 3)
        category = "Accessory";
    else
        throw new Error(`Unknow category ${ category }`);

    if (productID == null)
        requestUrl = `../../server/provider/AddProduct.php?`;
    else
        requestUrl = `../../server/provider/editProduct.php?id=${ productID }&`;

    requestUrl += `name=${ name }&category=${ category }&price=${ price }&qty=${ count }`;

    (async () => {
        try
        {
            const res = await fetch(requestUrl);
            if (!res.ok)
                throw new Error(`Unable to access server, ${ res.status } ${ res.statusText }`);

            const msg = await res.text();
            if (msg.match(/^Error:/)) throw new Error(msg.replace(/^Error: /g, ""));
            if (msg.length != 0) throw new Error(msg);
        }
        catch (error)
        {
            console.log(error);
            alert("Submit failed due to an error");
        }
    })();

    alert("Form submited!");
    location.href = sessionStorage.getItem("prev-url");
    cleanup();
}

async function onDelete()
{
    const id = sessionStorage.getItem("edit-product");
    if (id == null) return;
    if (confirm("Operation can't be undo! Are you sure to proceed?"))
    {
        try
        {
            const res = await fetch(`../../server/provider/delProduct.php?id=${ id }`);
            
            if (!res.ok)
                throw new Error(`Operation failed, ${ res.status } ${ res.statusText }`);

            const msg = await res.text();
            if (msg.length > 0) throw new Error(msg);
            alert("Item deleted");
            location.href = sessionStorage.getItem("prev-url");
            cleanup();
        }
        catch (error)
        {
            console.log(error);
            alert(error);
        }
    }
}

function onCancel()
{
    cleanup();
    history.back();
}

function cleanup()
{
    sessionStorage.removeItem("prev-url");
    sessionStorage.removeItem("edit-product");
}

async function onLoaded()
{
    if (sessionStorage.getItem("prev-url") == null)
    {
        alert("Error: data uncompleted");
        history.back();
    }

    const productID = sessionStorage.getItem("edit-product");
    if (productID != null)
    {
        let product = null;

        try
        {
            const res = await fetch("../../server/ClientServer/listProducts.php");
            
            if (!res.ok)
                throw new Error(`Failed to fetch ${ res.url } due to ${ res.status } ${ res.statusText }`);
            
            const data = await res.json();
            product = data.filter(itr => itr.id == productID)[0];
        }
        catch (error)
        {
            console.log(error);
            return;
        }

        let categoryID = 0;
        if (product.category.match(/^Game/)) categoryID = 1;
        else if (product.category.match(/^Console/)) categoryID = 2;
        else if (product.category.match(/^Accessory/)) categoryID = 3;

        document.querySelector("#name").value = product.name;
        document.querySelector("#price").value = product.price;
        document.querySelector("#category").value = categoryID;
        document.querySelector("#count").value = product.quantity;

        const title = `Edit ${ product.name }`;
        document.head.querySelector("title").innerHTML = title;
        document.querySelector("#page-title").innerHTML = title;
    }
    else
    {
        const title = "Add new product";
        document.head.querySelector("title").innerHTML = title;
        document.querySelector("#page-title").innerHTML = title;
    }
}