{
    if (sessionStorage.getItem("adminID") == null)
    {
        sessionStorage.setItem("msg", "Session expired");
        location.href = "./";
    }
}

function loadSidebar()
{
    let sidebar = document.querySelector("#main-sidebar");
    sidebar.classList.remove("w3-hide");
    sidebar.classList.add("animated", "slideInLeft");

    let greeting = document.querySelector("#greeting");
    let adminID = sessionStorage.getItem("adminID");
    let hours = (new Date()).getHours();

    if (hours >= (8 + 12))
        greeting.innerHTML = `Good Night! ${ adminID }!`;
    else if (hours >= (5 + 12))
        greeting.innerHTML = `Good Evening! ${ adminID }!`;
    else if (hours >= (1 + 12))
        greeting.innerHTML = `Good Afternoon! ${ adminID }!`;
    else
        greeting.innerHTML = `Good Morning! ${ adminID }!`;
}

function toggleNav(bars)
{
    let nav = document.querySelector("#main-nav");
    if (nav.classList.toggle("w3-hide-small"))
        bars.innerHTML = "<i class='fas fa-bars'></i>"
    else
        bars.innerHTML = "<i class='fas fa-times'></i>"
}

function toggleSidebar()
{
    let sidebar = document.querySelector("#main-sidebar");
    sidebar.classList.toggle("w3-hide-small");
    sidebar.classList.toggle("w3-hide-medium");
}

function logout()
{
    sessionStorage.clear();
    location.href = "./";
}

function toggleAccordions(selector, opener)
{
    let accordionBtn = document.querySelector(opener);
    let accordion = document.querySelector(selector);

    accordionBtn.classList.toggle("fa-caret-down");
    accordionBtn.classList.toggle("fa-caret-up");
    
    if (accordion.classList.contains("w3-hide"))
    {
        const animations = ["animated", "slideInDown"];
        const animationWorker = () => {
            accordion.removeEventListener("animationend", animationWorker);
            accordion.classList.remove(...animations);
        };
        accordion.classList.remove("w3-hide");
        accordion.addEventListener("animationend", animationWorker);
        accordion.classList.add(...animations);
    }
    else
    {
        const animations = ["animated", "slideOutUp"];
        const animationWorker = () => {
            accordion.classList.remove(...animations);
            accordion.removeEventListener("animationend", animationWorker);
            accordion.classList.add("w3-hide");
        };
        accordion.addEventListener("animationend", animationWorker);
        accordion.classList.add(...animations);
    }
}

function handleSearch()
{
    let keyword = document.querySelector("#main-searchbar").value;
    keyword = keyword.replace(/ /g, "");
    keyword = keyword.replace(/'/g, "'");
    location.href = `./products.html?subcat=search&query=${ keyword }`
}