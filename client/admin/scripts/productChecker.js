function formatStr(input, params)
{
    let str = input;
    params.forEach((param, idx) => {
        str = str.replace(new RegExp("\\{" + idx + "\\}", "g"), param.toString());
    });
    return str;
}

async function checkStock()
{
    try
    {
        let res = await fetch("../../server/ClientServer/listProducts.php");
        if (!res.ok)
            throw new Error(`Unable to fetch ${ res.url } with status ${ res.status } ${ res.statusText }`);

        let stock = await res.json();
        res = await fetch("./card.html");
        if (!res.ok)
            throw new Error(`Unable to fetch ${ res.url } with status ${ res.status } ${ res.statusText }`);

        let template = await res.text();
        checkGoingOutOfStock(stock, template);
        checkOutOfStock(stock, template);
    }
    catch (error)
    {
        console.log(error);
    }
}

function checkGoingOutOfStock(stock, template)
{
    let outOfStockPanel = document.querySelector("#going-out-of-stock");
    let outItems = stock.filter(item => ((item.quantity <= 10) && (item.quantity > 0)));

    if (outItems.length == 0)
    {
        outOfStockPanel.innerHTML = "&lt;No products are going to out of stock!&gt;";
        return;
    }

    outOfStockPanel.innerHTML = "";
    outItems.forEach(item => {
        outOfStockPanel.innerHTML += formatStr(template, [item.quantity, item.id, item.name, item.price]);
    });
}

function checkOutOfStock(stock, template)
{
    let outOfStockPanel = document.querySelector("#is-out-of-stock");
    let outItems = stock.filter(item => item.quantity == 0);

    if (outItems.length == 0)
    {
        outOfStockPanel.innerHTML = "&lt;No products are out of stock!&gt;";
        return;
    }

    outItems.forEach(item => {
        outOfStockPanel.innerHTML += formatStr(template, [item.quantity, item.id, item.name, item.price]);
    });
}