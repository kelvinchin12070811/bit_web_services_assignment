function formatStr(input, params)
{
    let str = input;
    params.forEach((param, idx) => {
        str = str.replace(new RegExp("\\{" + idx + "\\}", "g"), param.toString());
    });
    return str;
}

async function renderTemplate()
{
    await template.includeHTML();
    loadSidebar();
    let current = document.querySelector("#panels a:nth-child(2)");
    current.classList.add("w3-grey");

    let productAccor = document.querySelector("#products-panel");
    productAccor.classList.remove("w3-hide");
    const animations = ["animated", "slideInDown"];
    const animationendHandler = () => {
        productAccor.removeEventListener("animationend", animationendHandler);
        productAccor.classList.remove(...animations);
    };
    productAccor.addEventListener("animationend", animationendHandler);
    productAccor.classList.add(...animations);
}

async function routePages()
{
    /* Determine categories of page and display different page with it */
    const url = new URL(location.href);
    const curSubcat = url.searchParams.get("subcat");
    let pageIdx = 0;
    let products = null;
    let cardTemplate = null;
    try
    {
        let res = await fetch("../../server/ClientServer/listProducts.php");
        if (!res.ok)
            throw new Error(`Unable to fetch ${ res.url }, ${ res.status } ${ res.statusText }`);
        products = await res.text();
        if (products.match(/^Error/))
            throw new Error(products);

        products = JSON.parse(products);
    }
    catch (error)
    {
        console.log(error);
        return;
    }

    let placeholder = document.querySelector("#content-placeholder");

    try
    {
        let res = await fetch("./card.html");
        if (!res.ok)
            throw new Error(`Unable to fetch ${ res.url }, ${ res.status } ${ res.statusText }`);
        cardTemplate = await res.text();
    }
    catch (error)
    {
        console.log(error);
        return;
    }

    if (curSubcat === "all")
    {
        pageIdx = 1;
    }
    else if (curSubcat === "game")
    {
        products = products.filter(itr => itr.category == "Game");
        pageIdx = 2;
    }
    else if (curSubcat === "console")
    {
        products = products.filter(itr => itr.category == "Console");
        pageIdx = 3;
    }
    else if (curSubcat === "accessory")
    {
        products = products.filter(itr => itr.category == "Accessory");
        pageIdx = 4;
    }
    else if (curSubcat === "search")
    {
        const url = new URL(location.href);
        const keyword = url.searchParams.get("query");
        products = products.filter(itr => itr.name.match(new RegExp(keyword, "i")));
        pageIdx = 1;
    }

    placeholder.innerHTML = "";
    products.forEach(itr => {
        placeholder.innerHTML += formatStr(cardTemplate, [itr.quantity, itr.id, itr.name, itr.price]);
    });

    let curAccorItem = document.querySelector(`#products-panel a:nth-child(${ pageIdx })`);
    curAccorItem.classList.add("w3-white");
}