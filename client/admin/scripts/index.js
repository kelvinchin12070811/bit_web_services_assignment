{
    let msg = sessionStorage.getItem("msg");
    if (msg != null) runAnimation(msg);
    sessionStorage.clear();
}

function onSubmit()
{
    const username = document.querySelector("#admin-id").value;
    const password = document.querySelector("#password").value;

    (async () => {
        try
        {
            const url = `../../server/provider/verifyAdmin.php?name=${ username }&password=${ password }`;
            let res = await fetch(url);
            
            if (!res.ok)
                throw `Error on request: ${res.status} ${res.statusText}`;

            let data = await res.text();
            if (data.match(/^Error:/)) throw data;
            sessionStorage.setItem("adminID", username);
            location.href = "./dashboard.html";
        }
        catch (error)
        {
            document.querySelector("#password").value = "";
            runAnimation(error);
        }
    })();
}

function runAnimation(msg)
{
    let msgPanel = document.querySelector("#error-msg");
    let count = 0;
    msgPanel.innerHTML = msg;

    const onAnimateEnd = () => {
        switch (count)
        {
        case 0:
            msgPanel.classList.remove("w3-hide");
            msgPanel.classList.add("animated", "slideInUp");
            msgPanel.addEventListener("animationend", onAnimateEnd);
            msgPanel.addEventListener("webkitAnimationEnd", onAnimateEnd);
            break;
        case 1:
            setTimeout(onAnimateEnd, 3000);
            break;
        case 2:
            msgPanel.classList.remove("animated", "slideInUp");
            msgPanel.classList.add("animated", "slideOutDown");
            break;
        case 3:
            msgPanel.classList.remove("animated", "slideOutDown");
            msgPanel.classList.add("w3-hide");
        }
        
        count++;
    };

    onAnimateEnd();
}