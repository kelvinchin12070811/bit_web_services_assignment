const template = {
    includeHTML: async () => {
        let placeholders = document.querySelectorAll("[http-include]");
        if (placeholders == null) return;
        
        for (const itr of placeholders)
        {
            const src = itr.getAttribute("http-include");
            if (src == null) return;
            
            const req = await fetch(src);
            const res = await req.text();
            itr.innerHTML = res;
            itr.removeAttribute("http-include");
        }

        return;
    }
}