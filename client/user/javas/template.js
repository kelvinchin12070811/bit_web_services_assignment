const template = {
    includeHTML: function() {
        let placeholders = document.querySelectorAll("[http-include]");
        if (placeholders == null) return;
        placeholders.forEach(itr => {
            const src = itr.getAttribute("http-include");
            if (src == null) return;
            
            (async () => {
                const req = await fetch(src);
                const res = await req.text();
                itr.innerHTML = res;
                itr.removeAttribute("http-include");
            })();
        });
    }
}