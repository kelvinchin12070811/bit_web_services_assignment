function toggleNav(bars)
{
    let nav = document.querySelector("#main-nav");
    if (nav.classList.toggle("w3-hide-small"))
        bars.innerHTML = "<i class='fas fa-bars'></i>"
    else
        bars.innerHTML = "<i class='fas fa-times'></i>"
}

function onSearch()
{
    const query = document.querySelector("#main-searchbar").value;
    location.href = `./sresult.html?query=${ query }`;
}