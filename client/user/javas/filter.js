function myFunction(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

function closeFilterBar(bar) {
  document.querySelector(bar).classList.add("w3-hide-small");
}

function openFilterBar(bar) {
  document.querySelector(bar).classList.remove("w3-hide-small");
}

async function listProducts() {
  const template = 
`<div class="container w3-card product-card cat-{3}">
  <div class="image-container">
    <img src="../../server/Image/{2}.jpg" id="image">
  </div>
  <div class="desc">
    <div class="desc-grid">
      <span class="name-grid">{0}</span>
      <span class="price-grid">RM{1}</span>
    </div>

    <div
      class="w3-button w3-hover-green w3-border w3-border-green"
      id="buy-button"
      onclick="addToCart({2})"
    >
      Add to Cart
    </div>
  </div>
</div>`;

  const res = await fetch("../../server/ClientServer/listProducts.php");
  if (!res.ok) throw new Error(`Failed to connected to server, ${ res.status } ${ res.statusText }`);

  let data = await res.text();
  if (data.match(/^Error/)) throw new Error(data);
  
  data = JSON.parse(data);

  const formatStr = (input, params) => {
    let str = input;
    params.forEach((param, idx) => {
        str = str.replace(new RegExp("\\{" + idx + "\\}", "g"), param.toString());
    });
    return str;
  };

  data = data.filter(itr => itr.quantity != 0);

  let placeholder = document.querySelector("#products");
  data.forEach(itr => {
    placeholder.innerHTML += formatStr(template, [itr.name, itr.price, itr.id, itr.category]);
  });

  let cat = (new URL(location.href)).searchParams.get("cat");
  sessionStorage.setItem("cat", cat);
  let child = placeholder.querySelectorAll(".product-card");
  child.forEach(itr => {
    const isCat = itr.querySelector(".name-grid").innerHTML.match(new RegExp(cat, "i"));
    if (!isCat)
      itr.classList.add("w3-hide");
  });
}

function filterByPrice(checkbox, rules) {
  let products = document.querySelectorAll(".product-card");
  products.forEach(itr => {
    if (itr.querySelector(".name-grid").innerHTML.match(new RegExp(sessionStorage.getItem("cat"), "i")))
    {
      const price = itr.querySelector(".price-grid").innerHTML.replace(/RM/g, "");
      if (!rules(parseInt(price.replace(/,/g, ""))))
      {
        if (checkbox.checked == true)
          itr.classList.add("w3-hide");
        else
          itr.classList.remove("w3-hide");
      }
    }
  });
}

function filterByCat(checkbox, cat)
{
  let products = document.querySelectorAll(".product-card");
  products.forEach(itr => {
    if (itr.querySelector(".name-grid").innerHTML.match(new RegExp(sessionStorage.getItem("cat"), "i")))
    {
      if (!itr.classList.contains(`cat-${ cat }`))
      {
        if (checkbox.checked == true)
          itr.classList.add("w3-hide");
        else
          itr.classList.remove("w3-hide");
      }
    }
  });
}