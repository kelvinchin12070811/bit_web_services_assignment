const cartItemTemplate = `<div class="w3-card" id="cart-image">
<img src="../../server/Image/{0}.jpg" style="height: 100%;width: 100%;">
</div>

<div class="desc-container" id="cart-desc">
<span class="w3-half w3-padding" style="width: 100%;height: 40%" id="name-cart">{1}</span>
<span class="w3-half w3-padding" style="width: 100%;height: 40%" id="price-cart">RM{2}</span>
</div>

<button
  class="w3-border w3-border-red w3-hover-red"
  id="delete-item"
  onclick="removeFromCart({0})"
>
  <i class="fas fa-times"></i>
</button>`;

function closeCartBar(bar) {
  document.querySelector(bar).classList.add("w3-hide");
  document.querySelector("#cart-panel").innerHTML = "";
}

async function openCartBar(bar) {
  const formatStr = (input, params) => {
    let str = input;
    params.forEach((param, idx) => {
        str = str.replace(new RegExp("\\{" + idx + "\\}", "g"), param.toString());
    });
    return str;
  };

  let res =
    await fetch(`../../server/provider/getCartItem.php?userid=${ sessionStorage.getItem("user-id") }`);
  if (!res.ok) throw new Error(`Unable to connect to server, ${ res.status } ${ res.statusText }`);
  let dataCart = await res.text();
  if (dataCart.match(/^Error/)) throw new Error(data);
  dataCart = JSON.parse(dataCart);

  res = await fetch(`../../server/ClientServer/listProducts.php`);
  if (!res.ok) throw new Error(`Unable to connect to server, ${ res.status } ${ res.statusText }`);
  let dataProducts = await res.json();
  let cartPanel = document.querySelector("#cart-panel");
  cartPanel.innerHTML = "";

  for (const cartItem of dataCart)
  {
    let product = dataProducts.find(value => (value.id == cartItem.productID));
    cartPanel.innerHTML += formatStr(cartItemTemplate, [product.id, product.name, product.price]);
  }

  if (bar == null) return;
  document.querySelector(bar).classList.remove("w3-hide");
}

async function addToCart(productId) {
  const res = await fetch(`../../server/ClientServer/buyItem.php?id=${ productId }&` +
    `memberID=${ sessionStorage.getItem("user-id") }&quantity=1`);
  if (!res.ok) throw new Error(`Unable to connect to server, ${ res.status } ${ res.text() }`);
  const msg = await res.text();
  if (msg.length > 0) throw new Error(msg);
  alert("Item added to cart");
}

async function removeFromCart(productId) {
  const userid = sessionStorage.getItem("user-id");
  const url = `../../server/ClientServer/removeCartItem.php?userid=${ userid }&productid=${ productId }`;
  const res = await fetch(url);
  if (!res.ok) throw new Error(`Unable to connect to server, ${ res.status } ${ res.statusText }`);
  const msg = await res.text();
  if (msg.length > 0) throw new Error(msg);
  openCartBar();
}