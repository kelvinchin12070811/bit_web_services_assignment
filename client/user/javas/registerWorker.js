async function onCreateClicked()
{
    try
    {
        const username = document.querySelector("#inputname").value;
        const password = document.querySelector("#inputpassword").value;
        const email = document.querySelector("#inputemail").value;
        const res = await fetch(`../../server/ClientServer/addUser.php?name=${ username }&address=${ email }&`
            + `email=${email}&password=${password}`);
        if (!res.ok) throw new Error(`Failed to connect to server, ${ res.status } ${ res.statusText }`);
        const msg = await res.text();
        if (msg.length > 0) throw new Error(msg);
        alert("Account created!");
        location.href = "./index.html";
    }
    catch (error)
    {
        alert(error.message);
        console.log(error);
    }
}