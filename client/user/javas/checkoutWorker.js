async function onLoad()
{
    const formatStr = (input, params) => {
        let str = input;
        params.forEach((param, idx) => {
            str = str.replace(new RegExp("\\{" + idx + "\\}", "g"), param.toString());
        });
        return str;
    };
    const template = `<p><a href="#">{0}</a> <span class="price">RM{1}</span></p>`;
    let cartItems = document.querySelector("#items");
    let subTotal = document.querySelector("#total");
    let total = 0;

    try
    {
        const userid = sessionStorage.getItem("user-id");
        const resProducts = await fetch("../../server/ClientServer/listProducts.php");
        if (!resProducts.ok)
            throw new Error(`Unable to connect to server, ${ resProducts.status } ${resProducts.statusText}`);
        let products = await resProducts.text();

        const resCart = await fetch(`../../server/ClientServer/getCartItems.php?userid=${ userid }`);
        if (!resCart.ok)
            throw new Error(`Unable to connect to server, ${ resCart.status } ${ resCart.statusText }`);
        let cart = await resCart.text();

        if (products.match(/^Error/)) throw new Error(products);
        if (cart.match(/^Error/)) throw new Error(cart);

        products = JSON.parse(products);
        cart = JSON.parse(cart);

        document.querySelector("#item-count").innerHTML = cart.length;
        for (const cartItem of cart)
        {
            const curProduct = products.find(itr => (itr.id == cartItem.productID));
            let productName = curProduct.name;

            if (productName.length > 21)
                productName = productName.substring(0, 21);

            cartItems.innerHTML += formatStr(template, [productName, curProduct.price]);
            total += parseFloat(curProduct.price);
        }

        subTotal.innerHTML = total;
    }
    catch (error)
    {
        alert(error.message);
        console.log(error);
    }
}

async function checkout()
{
    try
    {
        const userid = sessionStorage.getItem("user-id");
        const res = await fetch(`../../server/ClientServer/getCartItems.php?userid=${ userid }`);
        if (!res.ok) throw new Error(`Unable to connect to server, ${ res.status } ${ res.statusText }`);

        let cart = await res.json();
        for (const cartItem of cart)
        {
            const removeReq = await fetch(`../../server/ClientServer/removeCartItem.php?` +
                `userid=${ userid }&productid=${ cartItem.productID }`);

            if (!removeReq.ok)
            {
                throw new Error(`Unable to connect to server, ${ removeReq.status }` +
                    ` ${ removeReq.statusText }`);
            }
        }
        alert("Payment successful, returning to home page...");
        location.href = "./main.html";
    }
    catch (error)
    {
        alert(error.message);
        console.log(error);
    }
}