async function onLoad()
{
    const template = `<div class="w3-card">
    <div class="image-container">
        <img src="../../Server/Image/{0}.jpg" id="image">
    </div>
    <div class="desc">
        <div class="desc-grid">
            <span id="name-grid">{1}</span>
            <span id="price-grid">RM{2}</span>
        </div>

        <div
            class="w3-button w3-hover-green w3-border w3-border-green"
            id="buy-button"
            onclick="addToCart({0})"
        >
            Add to Cart
        </div>
    </div>
</div>`;
    const formatStr = (input, params) => {
        let str = input;
        params.forEach((param, idx) => {
            str = str.replace(new RegExp("\\{" + idx + "\\}", "g"), param.toString());
        });
        return str;
    };

    let container = document.querySelector("#container");
    const url = new URL(location.href);
    const query = url.searchParams.get("query");
    try
    {
        const res = await fetch("../../server/ClientServer/listProducts.php");
        if (!res.ok) throw new Error(`Unable to connect to server, ${ res.status } ${ res.statusText }`);
        let products = await res.json();
        products = products.filter(product => (product.name.match(new RegExp(query, "i"))));
        for (const product of products)
        {
            container.innerHTML += formatStr(template, [product.id, product.name, product.price]);
        }
    }
    catch (error)
    {
        alert(error.message);
        console.log(error);
    }
}