function checkLogin()
{
    if (sessionStorage.getItem("user-id") != null) return;
    alert("Session expired!");
    location.href = "./index.html";
}

async function handleLogin()
{
    try
    {
        const userID = document.querySelector("#inputname").value;
        const key = document.querySelector("#inputpassword").value;

        const res = await fetch(`../../server/provider/verifyMember.php?name=${ userID }&password=${ key }`);
        if (!res.ok) throw new Error(`Failed to connect to server, ${ res.status } ${ res.statusText }`);
        const msg = await res.text();
        if (!msg.match(/^userid:/)) throw new Error(msg);
        sessionStorage.setItem("user-id", msg.replace(/^userid:/, ""));
        location.href = "./main.html";
    }
    catch (error)
    {
        alert(error.message);
        console.log(error);
    }
}

function logout()
{
    sessionStorage.clear();
    location.href = "./index.html";
}