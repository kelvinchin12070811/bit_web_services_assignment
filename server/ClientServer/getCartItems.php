<?php
if (!isset($_GET["userid"]))
{
    echo "Error: userid is required";
    exit;
}

$userID = $_GET["userid"];
$req = curl_init("http://localhost/server/provider/getCartItem.php?userid=" . $userID);
curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($req);
curl_close($req);

if ($result == false)
{
    echo "Error: failed to fetch from server";
    exit;
}

$data = json_decode($result);
$cartItems = [];
foreach($data as $itr)
{
    $cartItems[] = (object)[
        "productID" => $itr->productID,
        "quantity" => $itr->quantity
    ];
}

echo json_encode($cartItems);