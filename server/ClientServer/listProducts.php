<?php
function httpGet($url){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($ch);
	curl_close($ch);
	return $output;
}

$jsonProducts = httpGet("http://localhost/Server/provider/AllProduct.php");

$listProducts = json_decode($jsonProducts);//decode the json
$profit_margin = 1.10; //profit
$product = array();


foreach ($listProducts as $key1 => $value1) {//for the rows (multi dimensional array)
	$tmp_product = [];	
	foreach ($value1 as $key2 => $value2) {//for the columns
		$PriceNum = $value2;
		if($key2 == "price")
		{
			$num = $value2*$profit_margin;
			$PriceNum = number_format($num, 2);
			$tmp_product [$key2] = $PriceNum;
		}
		else{ 
			$tmp_product [$key2] = $PriceNum;
		}
	}
	$product[] = $tmp_product;
}

header("Content-Type: application/json");
	echo json_encode ($product);
