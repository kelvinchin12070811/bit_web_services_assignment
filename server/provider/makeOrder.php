<?php
function httpGet($url){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($ch);
	curl_close($ch);
	return $output;
}

$servername = "localhost";
$username = "ws_user";
$password = "12345";
$dbname = "ws_user";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    echo("Error: Connection failed: " . $conn->connect_error);
    exit;
}

$productID = $_GET["itemID"];
$userID = $_GET["memberID"];
$qty = $_GET["qty"];

$sql = "INSERT INTO `order_table` (`orderID`,`memberID`, `productID`,`quantity`) VALUES (NULL, $userID, $productID, $qty);";

if ($conn->query($sql) === TRUE) {
    
    $setQty = httpGet("http://localhost/Server/provider/setQty.php?id=".$productID."&qty=".$qty);
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
