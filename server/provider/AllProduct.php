 <?php
$db_user = "ws_user";
$db_server = "localhost";
$db_pass = "12345";
$db_name = "ws_user";

$conn = new mysqli($db_server, $db_user, $db_pass, $db_name);// create connection to the database
$sql = "SELECT * FROM products";//select from the database
$result = $conn->query($sql);//to perform query to the database(extract information)
$products = array();

if($result->num_rows > 0)//if number of row is more than 0 
{
	
	while ($row = $result->fetch_assoc()){//fetch row one by one
		$id = $row["id"];
		foreach($row as $key => $value){
			$products[$id][$key] = $value;
		}
	}
}

echo json_encode($products);
