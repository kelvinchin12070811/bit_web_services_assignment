-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2020 at 03:16 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ws_user`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adminID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `memberID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_table`
--

CREATE TABLE `order_table` (
  `orderID` int(11) NOT NULL,
  `memberID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `category`, `price`, `quantity`) VALUES
(1, 'Magicolors (Switch)', 'Game', '4.00', 100),
(2, 'Sniper (Switch)', 'Game', '39.99', 100),
(3, 'AFL Revolution 2(Switch)', 'Game', '199.99', 100),
(4, 'Hyper Jam (Switch)', 'Game', '59.00', 100),
(5, 'OMG Police (Switch)', 'Game', '20.00', 100),
(6, 'Assassin\'s Creed Syndicate (PS3)', 'Game', '89.99', 100),
(7, 'Minecraft (PS3)', 'Game', '91.00', 100),
(8, 'GTA Online (PS3)', 'Game', '119.00', 100),
(9, 'FIFA 20 (PS4)', 'Game', '179.99', 100),
(10, 'Concrete Genie (PS4)', 'Game', '129.00', 100),
(11, 'Call of Duty: Modern Warfare (PS4)', 'Game', '132.00', 100),
(12, 'Nintendo Switch Lite', 'Console', '1259.00', 50),
(13, 'Nintendo Switch', 'Console', '1758.00', 50),
(14, 'Switch Pro Controller', 'Accessory', '259.89', 50),
(15, 'Nintendo-Switch Case', 'Accessory', '26.00', 50),
(16, 'Nintendo Switch USB-C Charger', 'Accessory', '277.00', 50),
(17, 'Nintendo Switch Hard Case', 'Accessory', '45.00', 50),
(18, 'Switch/PS Gaming Headset', 'Accessory', '123.79', 50),
(19, 'PS3', 'Console', '599.00', 50),
(20, 'PS3 Flight Stick', 'Accessory', '475.00', 50),
(21, 'PS3 Travel Case', 'Accessory', '130.00', 50),
(22, 'PS4', 'Console', '1390.00', 50),
(23, 'PS Playstation VR', 'Console', '1509.00', 50),
(24, 'PS4 Case', 'Accessory', '42.00', 50),
(25, 'PS4 Controller ', 'Accessory', '139.99', 50);

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminID`, `name`, `password`) VALUES
(1, 'Kelvinchin', 'sakurasamadaisuki'),
(2, 'Caleb99', 'csisthebest'),
(3, 'Menlong', 'ethenplsdont'),
(4, 'Irisleong', 'hiethen');

--
-- Indexes for dumped tables
--


--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminID`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`memberID`);

--
-- Indexes for table `order_table`
--
ALTER TABLE `order_table`
  ADD PRIMARY KEY (`orderID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `memberID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_table`
--
ALTER TABLE `order_table`
  MODIFY `orderID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
